package br.com.vivo.scheduler.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.vivo.scheduler.entity.Job;
import br.com.vivo.scheduler.repository.JobRepository;
import br.com.vivo.scheduler.service.impl.SchedulerServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class SchedulerServiceTest {

	@InjectMocks
	private SchedulerService schedulerService = new SchedulerServiceImpl();

	@Mock
	private JobRepository jobRepository;

	@Autowired
	private ObjectMapper mapper;

	@Test
	void testShouldReturnListJobsIdsWithGroupMaxTime8Hours() throws Exception{
		// setup mock <<
		// massa de dados <<
		List<Job> mockListJobs = new ArrayList<>();

		mockListJobs.add(new Job(1, "Importação de arquivos de fundos",
				LocalDateTime.of(2019, 11, 10, 12, 0, 0), 2));

		mockListJobs.add(new Job(2, "Importação de dados da Base Legada",
				LocalDateTime.of(2019, 11, 11, 12, 0, 0), 4));

		mockListJobs.add(new Job(3, "Importação de dados de integração",
				LocalDateTime.of(2019, 11, 11, 8, 0, 0), 6));

		when(jobRepository.findAll()).thenReturn(mockListJobs);
		// >> setup mock

		// given <<
		LocalDateTime dateStart = LocalDateTime.of(2019, 11, 10, 9, 0, 0);
		LocalDateTime dateEnd = LocalDateTime.of(2019, 11, 11, 12, 0, 0);
		Integer groupTime = 8;
		// >> given

		// when <<
		Optional<List<List<Job>>> jobs = schedulerService.listJobsByGroupTime(dateStart, dateEnd, groupTime);
		// >> when
		
		// then <<
		assertTrue(jobs.isPresent());

		assertEquals(2, jobs.get().size());

		String result = getJobsIdsToString(jobs);
		assertEquals("[[1,3],[2]]", result);
		// >> then
	}

	@Test
	void testShouldReturnEmptyGivePeriodIsAfterPeriod() throws Exception{
		// setup mock <<
		// massa de dados <<
		List<Job> mockListJobs = new ArrayList<>();

		mockListJobs.add(new Job(1, "Importação de arquivos de fundos",
				LocalDateTime.of(2019, 11, 9, 12, 0, 0), 2));

		when(jobRepository.findAll()).thenReturn(mockListJobs);
		// >> setup mock

		// given <<
		LocalDateTime dateStart = LocalDateTime.of(2019, 11, 10, 9, 0, 0);
		LocalDateTime dateEnd = LocalDateTime.of(2019, 11, 11, 12, 0, 0);
		Integer groupTime = 8;
		// >> given

		// when <<
		Optional<List<List<Job>>> jobs = schedulerService.listJobsByGroupTime(dateStart, dateEnd, groupTime);
		// >> when
		
		// then <<
		assertFalse(jobs.isPresent());
		// >> then
	}

	@Test
	void testShouldReturnEmptyGivePeriodIsBeforePeriod() throws Exception{
		// setup mock <<
		// massa de dados <<
		List<Job> mockListJobs = new ArrayList<>();

		mockListJobs.add(new Job(1, "Importação de arquivos de fundos",
				LocalDateTime.of(2019, 11, 13, 12, 0, 0), 2));

		when(jobRepository.findAll()).thenReturn(mockListJobs);
		// >> setup mock

		// given <<
		LocalDateTime dateStart = LocalDateTime.of(2019, 11, 10, 9, 0, 0);
		LocalDateTime dateEnd = LocalDateTime.of(2019, 11, 11, 12, 0, 0);
		Integer groupTime = 8;
		// >> given

		// when <<
		Optional<List<List<Job>>> jobs = schedulerService.listJobsByGroupTime(dateStart, dateEnd, groupTime);
		// >> when
		
		// then <<
		assertFalse(jobs.isPresent());
		// >> then
	}

	@Test
	void testShouldReturnJobsInPeriod() throws Exception{
		// setup mock <<
		// massa de dados <<
		List<Job> mockListJobs = new ArrayList<>();

		mockListJobs.add(new Job(1, "Importação de arquivos de fundos",
				LocalDateTime.of(2019, 11, 13, 12, 0, 0), 2));
		
		mockListJobs.add(new Job(2, "Importação de dados da Base Legada",
				LocalDateTime.of(2019, 11, 11, 12, 0, 0), 4));

		when(jobRepository.findAll()).thenReturn(mockListJobs);
		// >> setup mock

		// given <<
		LocalDateTime dateStart = LocalDateTime.of(2019, 11, 10, 9, 0, 0);
		LocalDateTime dateEnd = LocalDateTime.of(2019, 11, 11, 12, 0, 0);
		Integer groupTime = 8;
		// >> given

		// when <<
		Optional<List<List<Job>>> jobs = schedulerService.listJobsByGroupTime(dateStart, dateEnd, groupTime);
		// >> when
		
		// then <<
		assertTrue(jobs.isPresent());
		
		assertEquals(1, jobs.get().size());
		
		String result = getJobsIdsToString(jobs);
		assertEquals("[[2]]", result);
		// >> then
	}

	@Test
	void testShouldReturnJobsInPeriodIntoOfMaxTimeExecutation() throws Exception{
		// setup mock <<
		// massa de dados <<
		List<Job> mockListJobs = new ArrayList<>();

		mockListJobs.add(new Job(1, "Importação de arquivos de fundos",
				LocalDateTime.of(2019, 11, 10, 13, 0, 0), 4));
		
		mockListJobs.add(new Job(2, "Importação de dados da Base Legada",
				LocalDateTime.of(2019, 11, 10, 13, 0, 0), 2));

		when(jobRepository.findAll()).thenReturn(mockListJobs);
		// >> setup mock

		// given <<
		LocalDateTime dateStart = LocalDateTime.of(2019, 11, 10, 9, 0, 0);
		LocalDateTime dateEnd = LocalDateTime.of(2019, 11, 11, 12, 0, 0);
		Integer groupTime = 8;
		// >> given

		// when <<
		Optional<List<List<Job>>> jobs = schedulerService.listJobsByGroupTime(dateStart, dateEnd, groupTime);
		// >> when
		
		// then <<
		assertTrue(jobs.isPresent());

		assertEquals(1, jobs.get().size());
		
		String result = getJobsIdsToString(jobs);
		assertEquals("[[1]]", result);
		// >> then
	}

	private String getJobsIdsToString(Optional<List<List<Job>>> jobs) throws Exception{
		List<List<Integer>> ids = jobs.get().stream()
						 .map(e -> e.stream()
							.map(e1 -> e1.getId())
							.collect(Collectors.toList())
						 )
						 .collect(Collectors.toList());
		return mapper.writeValueAsString(ids);
	}
}
