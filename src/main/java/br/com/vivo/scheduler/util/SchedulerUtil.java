package br.com.vivo.scheduler.util;

import java.time.LocalDateTime;

public class SchedulerUtil {

    public static boolean validatePeriod(LocalDateTime maxDateExecution, LocalDateTime currentDate, LocalDateTime endDate){
        return maxDateExecution.isBefore(currentDate) || maxDateExecution.isAfter(endDate);
    }
    
}