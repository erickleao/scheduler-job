package br.com.vivo.scheduler.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import br.com.vivo.scheduler.entity.Job;

public interface SchedulerService {
    
    Optional<List<List<Job>>> listJobsByGroupTime(LocalDateTime startDate, LocalDateTime endDate, Integer groupTime);

}