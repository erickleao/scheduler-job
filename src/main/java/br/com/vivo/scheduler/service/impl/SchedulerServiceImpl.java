package br.com.vivo.scheduler.service.impl;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vivo.scheduler.entity.Job;
import br.com.vivo.scheduler.repository.JobRepository;
import br.com.vivo.scheduler.service.SchedulerService;
import br.com.vivo.scheduler.util.SchedulerUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class SchedulerServiceImpl implements SchedulerService {

    @Autowired
    private JobRepository jobRepository;

    @Override
    public Optional<List<List<Job>>> listJobsByGroupTime(LocalDateTime startDate, LocalDateTime endDate,
            Integer groupTime) {
        log.info("Start process listJobsByGroupTime <<");

        // variable assist for logical business
        List<List<Job>> schedulingJobs = new ArrayList<>();

        int currentMaxTimeDay = 0;
        LocalDateTime currentDate = startDate;

        // return all jobs
        List<Job> jobs = jobRepository.findAll();

        // order by maxExecution
        List<Job> jobsSorted = jobs.stream()
                                   .sorted(comparing(Job::getMaxExecutionDate))
                                   .collect(toList());

        for (Job job : jobsSorted) {
            // set variables assist for manipulation of business rules
            currentMaxTimeDay += job.getEstimatedTime().intValue();
            currentDate = currentDate.plusHours(job.getEstimatedTime());

            // analyze the maximum execution date has been exceeded
            if(SchedulerUtil.validatePeriod(job.getMaxExecutionDate(), currentDate, endDate))
                continue;

            if(currentMaxTimeDay > groupTime){
                log.info("... currentMaxTimeDay is greater that groupTime");
                // trans next jobs for next day starting MIDNIGHT
                currentDate = currentDate.plusDays(1l)
                                         .with(LocalTime.MIDNIGHT);
                
                // analyze the current date execution date has been exceeded the date end of processs
                if(currentDate.isAfter(endDate))
                    break;
                
                schedulingJobs.add(new ArrayList<>());
                currentMaxTimeDay = job.getEstimatedTime().intValue();
            }

            if(schedulingJobs.isEmpty()){
                log.info("... set var schedulingJobs");
                List<Job> jobsGroup = new ArrayList<>();
                jobsGroup.add(job);
                schedulingJobs.add(jobsGroup);
            }else{
                log.info("... get schedulingJobs");
                schedulingJobs.get(schedulingJobs.size() - 1).add(job);
            }

        }

        log.info(">> End process listJobsByGroupTime");
        return !schedulingJobs.isEmpty()? Optional.of(schedulingJobs) : Optional.empty();

    }
}