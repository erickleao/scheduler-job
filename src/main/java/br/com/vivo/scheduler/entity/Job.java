package br.com.vivo.scheduler.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Job {
    
    @Id
    private Integer id;

    private String description;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime maxExecutionDate;

    private Integer estimatedTime;
}